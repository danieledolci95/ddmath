class RowIndexError(IndexError):
	"""Raise when the index of the row is greater than the amout of rows of the matrix."""

class ColIndexError(IndexError):
	"""Raise when the index of the column is greater than the amout of columns of the matrix."""

class MatrixSizeMismatchError(Exception):
	"""Raise when the matrices you are operating with are incompatible in respect to a specific 
	   operation due to different size"""

class UninvertibleMatrixError(Exception):
	"""Raise when the matrix cannot be inverted."""

class OverdetermindSystemError(Exception):
	"""Raise when the system is overdetermined."""

class UndeterminedSystemError(Exception):
	"""Raise when the system is undetermined."""

