"""
.. module:: ddMatrix
    :synopsis: Module for matrix math
 
.. moduleauthor:: Daniele Dolci<danieledolci95@gmail.com>
"""
from ddErrors import RowIndexError
from ddErrors import ColIndexError
from ddErrors import MatrixSizeMismatchError
from ddErrors import UninvertibleMatrixError
from ddErrors import OverdetermindSystemError
from ddErrors import UndeterminedSystemError

class Matrix(object):
	""" Matrix class """

	EPSILON = 1.0e-15

	def __init__(self, elements=None, rows=1, cols=1):
		"""

		:param elements: This is the list of floats to build the matrix.
		:type elements: list
		:param rows: This is number of rows of the matrix.
		:type rows: int
		:param cols: This is number of columns of the matrix.
		:type cols: int

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>> 					  1.0, 7.0, 94.0, 76.0, 
			>>> 					  51.0, 78.0, 29.0, 63.0, 
			>>> 					  38.0, 42.0, 17.0, 9.0], 
			>>> 					  rows=4, 
			>>> 					  cols=4)
		"""


		self._rows = rows 
		self._cols = cols 

		self._matrix = [0.0 for i in range(self._rows*self._cols)] if elements is None else elements


	@property
	def rowscount(self):
		"""
		Returns the amount of rows. 

		:getter: the number of rows.
		:setter: Not implemented.
		:type: int
		"""
		return self._rows

	@property
	def colscount(self):
		"""
		Return the amount of columns.

		:getter: the number of columns.
		:setter: Not implemented.
		:type: int
		"""
		return self._cols

	@property
	def matrix(self):
		"""
		Returns the elements of the matrix as a flattened list

		:getter: the elements of the matrix as a flattened list.
		:setter: Not implemented.
		:type: list
		"""
		return self._matrix

	@property
	def rank(self):
		"""
		Returns the rank of the matrix

		:getter: the rank of the matrix.
		:setter: Not implemented.
		:type: int
		"""
		rank = 0
		echelon = self.gauss()
		for row in range(self._rows):
			for col in range(self._cols):
				if echelon[row, col] != 0:
					rank+=1
					break
		return rank

	def __str__(self):	
		"""
		Returns the prettified version of the list

		:returns: the prettified version of the list. 
		:rtype: str

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1
			23.0       12.0       45.0       61.0  
			1.0        7.0        94.0       76.0     
			51.0       78.0       29.0       63.0     
			38.0       42.0       17.0       9.0   

		"""
		strings = ""
		for i in range(self._rows):
			temp = [str(round(x, 2)).ljust(10) for x in self._matrix[i*self._cols:(i*self._cols)+self._cols]]
			strings += " ".join(temp) + "\n"
		
		return strings

	def __add__(self, matrix):
		"""
		:param matrix: the matrix to be added. 
		:type matrix: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([1, 2, 3, 4, 5, 6, 7, 8, 9], rows=3, cols=3)
			>>> m2 = ddMatrix.Matrix([10, 11, 12, 13, 14, 15, 16, 17, 18], rows=3, cols=3)
			>>> print m1+m2
			11.0       13.0       15.0      
			17.0       19.0       21.0      
			23.0       25.0       27.0      
		"""
		if self._rows != matrix.rowscount or self._cols != matrix.colscount:
			raise MatrixSizeMismatchError("Addition can't be performed due to the different sizes of the matrices.")

		return Matrix([sum(x) for x in zip(self._matrix, matrix.matrix)], self._rows, self._cols)

	def __sub__(self, matrix):
		"""
		:param matrix: the matrix to be added. 
		:type matrix: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([1, 2, 3, 4, 5, 6, 7, 8, 9], rows=3, cols=3)
			>>> m2 = ddMatrix.Matrix([10, 11, 12, 13, 14, 15, 16, 17, 18], rows=3, cols=3)
			>>> print m2-m1
			9.0        9.0        9.0       
			9.0        9.0        9.0       
			9.0        9.0        9.0        
		"""
		if self._rows != matrix.rowscount or self._cols != matrix.colscount:
			raise MatrixSizeMismatchError("Subtraction can't be performed due to the different sizes of the matrices.")

		return self + matrix*-1

	def __mul__(self, matrix):
		"""
		:param matrix: the matrix to be multiplied or the coefficient.
		:type matrix: Matrix or int

		:Example:

			>>> m1 = ddMatrix.Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
			>>> m2 = ddMatrix.Matrix([7, 8, 9, 10, 11, 12], rows=3, cols=2)
			>>> print m1*m2
			58.0       64.0
			139.0      154.0     
		"""
		if isinstance(matrix, Matrix):
			if self._cols != matrix.rowscount:
				raise MatrixSizeMismatchError("Columns amount of first matrix does not match rows amount of second matrix.")
			else:
				new_matrix = Matrix(rows=self._rows, cols=matrix.colscount)
				for row in range(self._rows):
					row_vector = self.row(row+1)
					for col in range(matrix.colscount):
						col_vector = matrix.col(col+1)
						new_matrix[row, col] = sum([m1*m2 for m1, m2 in zip(row_vector.matrix, col_vector.matrix)])
				return new_matrix
		elif isinstance(matrix, int) or isinstance(matrix, float):
			return Matrix([x*matrix for x in self._matrix], self._rows, self._cols)


	def __getitem__(self, key):
		"""
		Returns the accessed element.

		:param key: the indeces of row and col of the element you want to access. 
		:type key: tuple
		:raise: RowIndexError, ColIndexError
		:returns: the accessed element. 
		:rtype: int or float

		:Example:

			>>> m1 = ddMatrix.Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
			>>> print m1[0, 2]
			3
		"""
		row, col = key
		if row >= self._rows:
			raise RowIndexError("Row out of range.")

		if col >= self._cols:
			raise ColIndexError("Col out of range.")

		return self._matrix[row*self._cols+col]

	def __setitem__(self, key, value):
		"""
		:param key: the indeces of row and col of the element you want to access. 
		:type key: tuple
		:param value: the value you want to assign to accessed element.
		:type value: int or float

		:Example:
		
			>>> m1 = ddMatrix.Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
			>>> print m1[0, 2]
			3
			>>> m1[0, 2] = 5
			>>> print m1[0, 2]
			5
		"""
		row, col = key
		if row > self._rows:
			raise RowIndexError("Row out of range.") 

		if col > self._cols:
			raise ColIndexError("Col out of range.")

		self._matrix[row*self._cols+col] = value

	def __eq__(self, matrix):
		"""
		Returns true if all elements of the 2 matrices are equals. 

		:param matrix: the matrix to compare to. 
		:type matrix: Matrix
		:returns: the result of comparison.
		:rtype: bool

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> m2 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1 == m2
			True
			>>> m2[2, 2] = 1
			>>> print m1 == m2
			False

		"""
		for e1, e2 in zip(self._matrix, matrix.matrix):
			if abs(e1 - e2) > self.EPSILON:
				return False
		return True

	def __ne__(self, matrix):
		"""
		Returns true if at least one element of the 2 matrices is different. 

		:param matrix: the matrix to compare to. 
		:type matrix: Matrix
		:returns: the result of comparison.
		:rtype: bool

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> m2 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1 != m2
			False
			>>> m2[2, 2] = 1
			>>> print m1 != m2
			True
		"""
		for e1, e2 in zip(self._matrix, matrix.matrix):
			if abs(e1 != e2) > self.EPSILON:
				return True
		return False

	@property
	def precision(self):
		"""
		Returns the precision set for the matrix. 

		:getter: the precision.
		:setter: int, the exponent of the scientific notation.
		:type: float
		"""
		return self.EPSILON

	@precision.setter
	def precision(self, exp):
		self.EPSILON = 1/10**exp

	def gauss(self):
		"""
		Performs a gaussian elimination.

		:returns: the row-reduced matrix. 
		:rtype: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1.gauss()
			51.0       78.0       29.0       63.0      
			0.0        5.47       93.43      74.76     
			0.0        0.0        427.75     349.33    
			0.0        0.0        0.0        -38.71    

		"""
		return self._gauss()[0]

	def _gauss(self):
		"""
		Performs a gaussian elimination.

		:returns: the row-reduced matrix and the coefficients for the determinant given by the amount of row-swaps. 
		:rtype: tuple

		"""
		gaussian_matrix = [x for x in self._matrix]
		final_coeff = -1
		for col in range(min(self._cols, self._rows)):
			max_row = col
			max_value = gaussian_matrix[col*self._cols+col]
			for row in range(col+1, self._rows):
				if gaussian_matrix[row*self._cols+col] > max_value:
					max_row = row
					max_value = gaussian_matrix[row*self._cols+col]
			
			if max_row != col:	
				for k, v in zip(range(max_row*self._cols, (max_row+1)*self._cols), range(col*self._cols, (col+1)*self._cols)):
					gaussian_matrix[k], gaussian_matrix[v] = gaussian_matrix[v], gaussian_matrix[k]	
					final_coeff *= -1
				
			for row in range(col+1, self._rows):
				try:
					coeff = gaussian_matrix[row*self._cols+col]/gaussian_matrix[col*self._cols+col]
				except ZeroDivisionError:
					coeff = 0.0

				for c in range(self._cols):
					if c == col:
						gaussian_matrix[row*self._cols+c] = 0
					else:
						gaussian_matrix[row*self._cols+c] -= coeff*gaussian_matrix[col*self._cols+c]

		return (Matrix(gaussian_matrix, self._rows, self._cols), final_coeff)

	def gaussjordan(self):
		"""
		Performs a Gauss-Jordan Elimination. Helpful if you first augment the matrix
		by the identity matrix. That way you can use this to perform operations such 
		as the inversion.

		:returns: the gauss-jordan elimination result Matrix. 
		:rtype: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> I = ddMatrix.Matrix.identity(4)
			>>> print m1
			23.0       12.0       45.0       61.0      
			1.0        7.0        94.0       76.0      
			51.0       78.0       29.0       63.0      
			38.0       42.0       17.0       9.0       
			>>> print m1.haugment(I)
			23.0       12.0       45.0       61.0       1.0        0.0        0.0        0.0       
			1.0        7.0        94.0       76.0       0.0        1.0        0.0        0.0       
			51.0       78.0       29.0       63.0       0.0        0.0        1.0        0.0       
			38.0       42.0       17.0       9.0        0.0        0.0        0.0        1.0       
			>>> print m1.haugment(I).gaussjordan()
			1.0        0.0        0.0        0.0        0.04       -0.02      -0.02      0.03      
			0.0        1.0        -0.0       0.0        -0.04      0.01       0.02       -0.01     
			0.0        0.0        1.0        0.0        -0.01      0.02       -0.01      0.02      
			0.0        0.0        0.0        1.0        0.02       -0.01      0.01       -0.03     

		"""
		gaussian_matrix = self.gauss()
		for col in range(self._rows-1, -1, -1):
			c = gaussian_matrix[col, col]
			for col2 in range(0, col):
				for x in range(self._cols-1, col-1, -1):
					gaussian_matrix[col2, x] -= gaussian_matrix[col, x]*gaussian_matrix[col2, col]/c

			gaussian_matrix[col, col] /= c
			for x in range(self._rows, self._cols):
				gaussian_matrix[col, x] /= c

		return Matrix(gaussian_matrix.matrix, self._rows, self._cols)

	def transpose(self):
		"""
		Perform a transposition operation. 

		:returns: the transposed Matrix. 
		:rtype: Matrix
		
		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1
			23.0       12.0       45.0       61.0      
			1.0        7.0        94.0       76.0      
			51.0       78.0       29.0       63.0      
			38.0       42.0       17.0       9.0       
			>>> print m1.transpose()
			23.0       1.0        51.0       38.0      
			12.0       7.0        78.0       42.0      
			45.0       94.0       29.0       17.0      
			61.0       76.0       63.0       9.0   
		"""
		transpose = [None for x in range(len(self._matrix))]
		for row in range(self._rows):
			for col in range(self._cols):
				transpose[col*self._rows+row] = self._matrix[row*self._cols+col]

		return Matrix(transpose, self._cols, self._rows)

	def inverse(self):
		"""
		Performs the inversion. 

		:returns: the inverted matrix.
		:rtype: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1.inverse()
			0.04       -0.02      -0.02      0.03      
			-0.04      0.01       0.02       -0.01     
			-0.01      0.02       -0.01      0.02      
			0.02       -0.01      0.01       -0.03     
			>>> print m1*m1.inverse()
			1.0        0.0        0.0        -0.0      
			0.0        1.0        -0.0       -0.0      
			0.0        0.0        1.0        0.0       
			0.0        0.0        0.0        1.0      
		"""
		identity = Matrix.identity(self._rows)
		gauss_jordan = self.haugment(identity).gaussjordan()
		reduced, inverse = gauss_jordan.hsplit(self._cols)

		return inverse

	def det(self):
		"""
		Returns the determinant.

		:returns: the determinant.
		:rtype: float

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1.det()
			4619881.999999999 # 
		"""
		matrix, coeff_det = self._gauss()
		for i in range(self._rows):
			coeff_det *= matrix[i, i]

		return coeff_det

	def haugment(self, matrix):
		"""
		Append horizontaly the matrix.

		:param matrix: the matrix to be appended.
		:type matrix: Matrix
		:raise: MatrixSizeMismatchError
		:returns: the augmented matrix. 
		:rtype: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> I = ddMatrix.Matrix.identity(4)
			>>> print m1
			23.0       12.0       45.0       61.0      
			1.0        7.0        94.0       76.0      
			51.0       78.0       29.0       63.0      
			38.0       42.0       17.0       9.0       
			>>> print m1.haugment(I)
			23.0       12.0       45.0       61.0       1.0        0.0        0.0        0.0       
			1.0        7.0        94.0       76.0       0.0        1.0        0.0        0.0       
			51.0       78.0       29.0       63.0       0.0        0.0        1.0        0.0       
			38.0       42.0       17.0       9.0        0.0        0.0        0.0        1.0       

		"""
		if self._rows != matrix.rowscount:
			raise MatrixSizeMismatchError("They have different amount of rows.")

		new_matrix = []
		for row in range(self._rows):
			for col in range(self._cols):
				new_matrix.append(self._matrix[row*self._cols+col])
			for col in range(matrix.colscount):
				new_matrix.append(matrix[row, col])

		return Matrix(new_matrix, self._rows, self._cols+matrix.colscount)

	def vaugment(self, matrix):
		"""

		Append vertically the matrix.

		:param matrix: the matrix to be appended.
		:type matrix: Matrix
		:raise: MatrixSizeMismatchError
		:returns: the augmented matrix. 
		:rtype: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> I = ddMatrix.Matrix.identity(4)
			>>> print m1		
			23.0       12.0       45.0       61.0      
			1.0        7.0        94.0       76.0      
			51.0       78.0       29.0       63.0      
			38.0       42.0       17.0       9.0  
			>>> print m1.vaugment(I)
			23.0       12.0       45.0       61.0      
			1.0        7.0        94.0       76.0      
			51.0       78.0       29.0       63.0      
			38.0       42.0       17.0       9.0       
			1.0        0.0        0.0        0.0       
			0.0        1.0        0.0        0.0       
			0.0        0.0        1.0        0.0       
			0.0        0.0        0.0        1.0       

		"""
		if self._cols != matrix.colscount:
			raise MatrixSizeMismatchError("They have different amount of cols.")

		new_matrix = []
		for row in range(self._rows):
			for col in range(self._cols):
				new_matrix.append(self._matrix[row*self._cols+col])
		for row in range(matrix.rowscount):
			for col in range(matrix.colscount):
				new_matrix.append(matrix[row, col])

		return Matrix(new_matrix, self._rows+matrix.rowscount, self._cols)

	def hsplit(self, subcols):
		"""
		Split the matrix into 2 matrices according the the subcols param. 

		:param subcols: the number of columns of first submatrix. 
		:type subcols: int
		:returns: the 2 submatrices of respectively subcols columns and colscount()-subcols. 
		:rtype: tuple

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> I = ddMatrix.Matrix.identity(4)
			>>> print m1.haugment(I)
			23.0       12.0       45.0       61.0       1.0        0.0        0.0        0.0       
			1.0        7.0        94.0       76.0       0.0        1.0        0.0        0.0       
			51.0       78.0       29.0       63.0       0.0        0.0        1.0        0.0       
			38.0       42.0       17.0       9.0        0.0        0.0        0.0        1.0       
			>>> print m1.haugment(I).hsplit(m1.colscount)[0]
			23.0       12.0       45.0       61.0      
			1.0        7.0        94.0       76.0      
			51.0       78.0       29.0       63.0      
			38.0       42.0       17.0       9.0       
			>>> print m1.haugment(I).hsplit(m1.colscount)[1]
			1.0        0.0        0.0        0.0       
			0.0        1.0        0.0        0.0       
			0.0        0.0        1.0        0.0       
			0.0        0.0        0.0        1.0       
		"""
		submatrix1 = []
		submatrix2 = []
		for row in range(self._rows):
			for col in range(self._cols):
				if col < subcols:
					submatrix1.append(self._matrix[row*self._cols+col])
				else:
					submatrix2.append(self._matrix[row*self._cols+col])

		return (Matrix(submatrix1, self._rows, subcols), Matrix(submatrix2, self._rows, self._cols-subcols))

	def vsplit(self, subrows):
		"""
		Split the matrix into 2 matrices according the the subrows param. 

		:param subrows: the number of columns of first submatrix. 
		:type subrows: int
		:returns: the 2 submatrices of respectively subrows columns and rowscount()-subrows. 
		:rtype: tuple
		
		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> I = ddMatrix.Matrix.identity(4)
			>>> print m1.vaugment(I)
			23.0       12.0       45.0       61.0      
			1.0        7.0        94.0       76.0      
			51.0       78.0       29.0       63.0      
			38.0       42.0       17.0       9.0       
			1.0        0.0        0.0        0.0       
			0.0        1.0        0.0        0.0       
			0.0        0.0        1.0        0.0       
			0.0        0.0        0.0        1.0 
			>>> print m1.vaugment(I).vsplit(m1.colscount)[0]
			23.0       12.0       45.0       61.0      
			1.0        7.0        94.0       76.0      
			51.0       78.0       29.0       63.0      
			38.0       42.0       17.0       9.0    
			>>> print m1.vaugment(I).vsplit(m1.colscount)[1]
			1.0        0.0        0.0        0.0       
			0.0        1.0        0.0        0.0       
			0.0        0.0        1.0        0.0       
			0.0        0.0        0.0        1.0       
		"""
		submatrix1 = []
		submatrix2 = []
		for row in range(self._rows):
			for col in range(self._cols):
				if row < subrows:
					submatrix1.append(self._matrix[row*self._cols+col])
				else:
					submatrix2.append(self._matrix[row*self._cols+col])

		return (Matrix(submatrix1, subrows, self._cols), Matrix(submatrix2, self._rows-subrows, self._cols))

	def row(self, row):
		"""
		Returns the n-th row as a row vector. 

		:param row: index of row to return. 
		:type row: int
		:returns: the n-th row as a row vector.
		:rtype: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1.row(2)
			1.0        7.0        94.0       76.0   

		"""
		return Matrix(self._matrix[(row-1)*self._cols:row*self._cols], 1, self._cols)

	def col(self, col):
		"""
		Returns the n-th row as a col vector. 

		:param col: index of row to return. 
		:type col: int
		:returns: the n-th row as a col vector.
		:rtype: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1.col(2)
			12.0
			7.0
			78.0
			42.0

		"""
		return Matrix([self._matrix[row*self._cols+col-1] for row in range(self._rows)], self._cols, 1)

	def distance(self):
		"""
		Returns the distance matrix, meaning the matrix with all the distances from each row to each other. 
		Such a matrix is  a symmetric square matrix. 

		:returns: the distance matrix.
		:rtype: Matrix

		:Example:

			>>> m1 = ddMatrix.Matrix([23.0, 12.0, 45.0, 61.0, 
			>>>			  1.0, 7.0, 94.0, 76.0, 
			>>>			  51.0, 78.0, 29.0, 63.0, 
			>>>			  38.0, 42.0, 17.0, 9.0], 
			>>>			  rows=4, 
			>>>			  cols=4)
			>>> print m1.distance()
			0.0        55.99      73.48      67.92     
			55.99      0.0        109.25     114.07    
			73.48      109.25     0.0        67.27     
			67.92      114.07     67.27      0.0       

		"""
		distance_matrix = []
		for i in range(self._rows):
			first_row = self.row(i+1)
			for k in range(self._rows):
				if k < i:
					distance_matrix.append(distance_matrix[k*self._rows+i])
					continue
				second_row = self.row(k+1)
				squared_distance = (first_row - second_row)
				for col in range(squared_distance.colscount):
					squared_distance[0, col] = squared_distance[0, col]**2
				distance_matrix.append(sum(squared_distance.matrix)**.5)

		return Matrix(distance_matrix, self._rows, self._rows)

	@staticmethod
	def identity(size):
		"""
		Generate an Identity matrix of rows x cols. 

		:param size: the size of the identity matrix. 
		:type size: int
		:returns: the identity matrix. 
		:rtype: Matrix

		:Example:

			>>> I = ddMatrix.Matrix.identity(4)
			>>> print I
			1.0        0.0        0.0        0.0       
			0.0        1.0        0.0        0.0       
			0.0        0.0        1.0        0.0       
			0.0        0.0        0.0        1.0   
		"""
		matrix = Matrix(rows=size, cols=size)
		for i in range(size):
			matrix[i, i] = 1

		return matrix

	@staticmethod
	def solve(coefficients, constants):
		"""
		Solves a linear system. 

		:param coefficients: the matrix of coefficients. 
		:type coefficients: Matrix
		:param constants: the matrix(col-vector) of constants.
		:type constants: Matrix
		:raise: ValueError
		:returns: the col-vector of the solution. 
		:rtype: Matrix

		:Example:

			>>> A = ddMatrix.Matrix([1.0, 2.0, 3.0, 2.0, -3.0, -5.0, -6.0, -8.0, 1.0], 3, 3)
			>>> B = ddMatrix.Matrix([-7.0, 9.0, -22.0], 3, 1)
			>>> print A	
			1.0        2.0        3.0       
			2.0        -3.0       -5.0      
			-6.0       -8.0       1.0       
			>>> print B
			-7.0      
			9.0       
			-22.0     
			>>> print ddMatrix.Matrix.solve(A, B)
			-1.0      
			3.0       
			-4.0     
		"""

		if coefficients.det() == 0:
			raise UndeterminedSystemError('The system has determinant 0, therefore it is undetermined.')

		return coefficients.inverse()*constants

	@classmethod
	def from_2dlist(cls, the_2dlist):
		"""
		Returns an instance of Matrix taking as parameteres a 2d list. 

		:param the_2dlist: the 2D list to use.
		:type the_2list: list
		:raise: ValueError
		:returns: the matrix built from the 2D list.
		:rtype: Matrix

		:Example:

			>>> the_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
			>>> m1 = ddMatrix.Matrix.from_2dlist(the_list)
			>>> print m1
			1.0        2.0        3.0       
			4.0        5.0        6.0       
			7.0        8.0        9.0       
		"""
		flattened_list = []
		col_count = len(the_2dlist[0])
		for row in the_2dlist:
			if len(row) != col_count:
				raise ValueError("Rows must have same amount of elements")
			for col in row:
				flattened_list.append(col)

		return cls(flattened_list, len(the_2dlist), col_count)
