ddMath package
==============

Submodules
----------

ddMath.ddErrors module
----------------------

.. automodule:: ddMath.ddErrors
    :members:
    :undoc-members:
    :show-inheritance:

ddMath.ddMatrix module
----------------------

.. automodule:: ddMath.ddMatrix
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __init__, __add__, __sub__, __mul__, __eq__, __ne__, __getitem__, __setitem__, __str__


Module contents
---------------

.. automodule:: ddMath
    :members:
    :undoc-members:
    :show-inheritance:
