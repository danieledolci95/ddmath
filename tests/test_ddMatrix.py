import pytest
from ddMath.ddMatrix import Matrix
from ddMath.ddErrors import RowIndexError
from ddMath.ddErrors import ColIndexError
from ddMath.ddErrors import MatrixSizeMismatchError
from ddMath.ddErrors import UninvertibleMatrixError
from ddMath.ddErrors import OverdetermindSystemError
from ddMath.ddErrors import UndeterminedSystemError


@pytest.fixture
def identity4():
    return Matrix.identity(4)

@pytest.fixture
def identity3():
    return Matrix.identity(3)

@pytest.fixture
def filled_matrix():
    return Matrix([23.0, 12.0, 45.0, 61.0, 
                            1.0, 7.0, 94.0, 76.0, 
                            51.0, 78.0, 29.0, 63.0, 
                            38.0, 42.0, 17.0, 9.0], 
                            rows=4, 
                            cols=4)

@pytest.fixture
def haugmented():
    return filled_matrix().haugment(identity4())

@pytest.fixture
def vaugmented():
    return filled_matrix().vaugment(identity4())

def test_rowscount():
    m1 = filled_matrix()
    assert m1.rowscount == 4

def test_colscount():
    m1 = filled_matrix()
    assert m1.colscount == 4

def test_matrix():
    m1 = filled_matrix()
    assert m1.matrix == [23.0, 12.0, 45.0, 61.0, 1.0, 7.0, 94.0, 76.0, 
                         51.0, 78.0, 29.0, 63.0, 38.0, 42.0, 17.0, 9.0] 

def test_rank():
    m1 = filled_matrix()
    assert m1.rank == 4

def test_str():
    m1 = filled_matrix()
    string = "23.0       12.0       45.0       61.0      \n"  \
            + "1.0        7.0        94.0       76.0      \n" \
            + "51.0       78.0       29.0       63.0      \n" \
            + "38.0       42.0       17.0       9.0       \n" 
    assert m1.__str__() == string

def test_add():
    m1 = Matrix([1, 2, 3, 4, 5, 6, 7, 8, 9], rows=3, cols=3)
    m2 = Matrix([10, 11, 12, 13, 14, 15, 16, 17, 18], rows=3, cols=3)
    result = Matrix([11, 13, 15, 17, 19, 21, 23, 25, 27], rows=3, cols=3)
    assert m1 + m2 == result

def test_add_exception1():
    m1 = Matrix([1, 2, 3, 4, 5, 6, 7, 8, 9], rows=3, cols=3)
    m2 = Matrix([10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], rows=4, cols=3)
    with pytest.raises(MatrixSizeMismatchError):
        m1 + m2

def test_sub():
    m1 = Matrix([1, 2, 3, 4, 5, 6, 7, 8, 9], rows=3, cols=3)
    m2 = Matrix([10, 11, 12, 13, 14, 15, 16, 17, 18], rows=3, cols=3)
    result = Matrix([9, 9, 9, 9, 9, 9, 9, 9, 9], rows=3, cols=3)
    assert m2 - m1 == result

def test_sub_exception1():
    m1 = Matrix([1, 2, 3, 4, 5, 6, 7, 8, 9], rows=3, cols=3)
    m2 = Matrix([10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], rows=4, cols=3)
    with pytest.raises(MatrixSizeMismatchError):
        m2 - m1

def test_mul_matrix():
    m1 =Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    m2 = Matrix([7, 8, 9, 10, 11, 12], rows=3, cols=2)
    result = Matrix([58.0, 64.0, 139.0, 154.0], rows=2, cols=2)
    assert m1 * m2 == result

def test_mul_float():
    m1 = Matrix([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], rows=2, cols=3)
    result = Matrix([.5, 1.0, 1.5, 2.0, 2.5, 3.0], rows=2, cols=3)
    assert m1*0.5 == result

def test_mul_exception1():
    m1 =Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    m2 = Matrix([7, 8, 9, 10, 11, 12], rows=2, cols=3)
    with pytest.raises(MatrixSizeMismatchError):
        m1*m2

def test_getitem():
    m1 = Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    assert m1[0, 2] == 3
            
def test_getitem_exception1():
    m1 = Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    with pytest.raises(RowIndexError):
        m1[2, 2]

def test_getitem_exception2():
    m1 = Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    with pytest.raises(ColIndexError):
        m1[0, 3]

def test_setitem():
    m1 = Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    m1[0, 2] = 5
    assert m1[0, 2] == 5

def test_setitem_exception1():
    m1 = Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    with pytest.raises(RowIndexError):
        m1[2, 2] == 10

def test_setitem_exception2():
    m1 = Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    with pytest.raises(ColIndexError):
        m1[0, 3] == 10

def test_eq_true():
    m1 =Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    m2 = Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    assert m1 == m2

def test_eq_false():
    m1 =Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    m2 = Matrix([7, 8, 9, 10, 11, 12], rows=2, cols=3)
    assert not(m1 == m2)

def test_neq_true():
    m1 =Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    m2 = Matrix([7, 8, 9, 10, 11, 12], rows=2, cols=3)
    assert m1 != m2
    
def test_neq_false():
    m1 =Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    m2 = Matrix([1, 2, 3, 4, 5, 6], rows=2, cols=3)
    assert not(m1 != m2)

def test_gauss():
    m1 = filled_matrix()
    result = Matrix([51.0, 78.0, 29.0, 63.0, 
        0, 5.470588235294118, 93.43137254901961, 74.76470588235294, 
        0.0, 0, 427.7491039426524, 349.33333333333337, 
        0.0, 0.0, 0, -38.71128353806705], 4, 4)
    assert m1.gauss() == result

def test_inverse():
    I = identity4()
    m1 = filled_matrix()
    print I
    print m1.inverse()*m1
    assert m1.inverse()*m1 == I
#TODO; precision error

def test_transpose():
    m1 = filled_matrix()
    result = Matrix([23.0, 1.0, 51.0, 38.0, 
        12.0, 7.0, 78.0, 42.0, 
        45.0, 94.0, 29.0, 17.0, 
        61.0, 76.0, 63.0, 9.0], 4, 4)
    assert m1.transpose() == result

def test_det():
    m1 = filled_matrix()
    assert m1.det() == 4619881.999999999

def test_haugment():
    m1 = haugmented()
    result = Matrix([23.0, 12.0, 45.0, 61.0, 1, 0.0, 0.0, 0.0, 
        1.0, 7.0, 94.0, 76.0, 0.0, 1, 0.0, 0.0, 
        51.0, 78.0, 29.0, 63.0, 0.0, 0.0, 1, 0.0, 
        38.0, 42.0, 17.0, 9.0, 0.0, 0.0, 0.0, 1], rows=4, cols=8)
    assert m1== result

def test_haugment_exception1():
    m1 = filled_matrix()
    I = identity3()
    with pytest.raises(MatrixSizeMismatchError):
        m1.haugment(I)

def test_vaugment():
    m1 = vaugmented()
    result = Matrix([23.0, 12.0, 45.0, 61.0, 
        1.0, 7.0, 94.0, 76.0, 
        51.0, 78.0, 29.0, 63.0, 
        38.0, 42.0, 17.0, 9.0, 
        1, 0.0, 0.0, 0.0, 
        0.0, 1, 0.0, 0.0, 
        0.0, 0.0, 1, 0.0, 
        0.0, 0.0, 0.0, 1], rows=8, cols=4)
    assert m1 == result

def test_vaugment_exception1():
    m1 = filled_matrix()
    I = identity3()
    with pytest.raises(MatrixSizeMismatchError):
        m1.vaugment(I)

def test_hsplit():
    m1 = filled_matrix()
    I = identity4()
    m1_aug = haugmented()
    assert m1_aug.hsplit(4)[0] == m1 and m1_aug.hsplit(4)[1] == I

def test_vsplit():
    m1 = filled_matrix()
    I = identity4()
    m1_aug = vaugmented()
    assert m1_aug.vsplit(4)[0] == m1 and m1_aug.vsplit(4)[1] == I

def test_row():
    m1 = filled_matrix()
    row = m1.row(2)
    result = Matrix([1.0, 7.0, 94.0, 76.0], rows=1, cols=4)
    assert row == result

def test_col():
    m1 = filled_matrix()
    col = m1.col(2)
    result = Matrix([12.0, 7.0, 78.0, 42.0], rows=4, cols=1)
    assert col == result

def test_distance():
    m1 = filled_matrix()
    result = Matrix([0.0, 55.991070716677676, 73.48469228349535, 67.91906948714772, 
                     55.991070716677676, 0.0, 109.24742559895863, 114.07015385279358, 
                     73.48469228349535, 109.24742559895863, 0.0, 67.26812023536856, 
                     67.91906948714772, 114.07015385279358, 67.26812023536856, 0.0], rows=4, cols=4)
    assert m1.distance() == result

def test_identity():
    I = identity4()
    result = Matrix([1, 0.0, 0.0, 0.0, 
                    0.0, 1, 0.0, 0.0, 
                    0.0, 0.0, 1, 0.0, 
                    0.0, 0.0, 0.0, 1], rows=4, cols=4)
    assert I == result

def test_solve():
    A = Matrix([1.0, 2.0, 3.0, 2.0, -3.0, -5.0, -6.0, -8.0, 1.0], 3, 3)
    B = Matrix([-7.0, 9.0, -22.0], 3, 1)
    result = Matrix([-1.0, 3.0, -4.0], rows=3, cols=1)
    col_res = Matrix.solve(A, B)
    print result.matrix
    print col_res.matrix
    assert col_res == result

#TODO; det == 0, precision error in solve

def test_from_2dlist():
    the_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    m1 = Matrix.from_2dlist(the_list)
    result = Matrix([1, 2, 3, 4, 5, 6, 7, 8, 9], rows=3, cols=3)
    assert m1 == result

def test_from_2dlist_exception1():
    the_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]]
    with pytest.raises(ValueError):
        m1 = Matrix.from_2dlist(the_list)

